# README #

This is a simple example of using the Apollo client 2.0 with Expo - it is based on the Expo Snack example, but updated to use 
Apollo 2.0 https://snack.expo.io/H1QdWZUjg 

Expo documentation: https://docs.expo.io/versions/latest/guides/using-graphql.html#using-apollo-client 

NOTE: a few install requirements do not appear to be up-to-date. 

In addition to the documented npm / yarn I also needed to add:
1. yarn add apollo-link-http

Then for the Imports:

import { ApolloProvider, graphql } from 'react-apollo';
import ApolloClient from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';

Prior to Apollo 2.0 the only import required 'react-apollo'
OLD import: import { gql, ApolloClient, createNetworkInterface, ApolloProvider, graphql } from 'react-apollo';




