import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import {Constants} from 'expo';
import { ApolloProvider, graphql } from 'react-apollo';
import ApolloClient from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';


export default class App extends React.Component {


    createClient() {
      return new ApolloClient({
        link: new HttpLink({ uri: 'http://api.githunt.com/graphql' }),
        cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
      });
    }

  render() {
    return (
      <ApolloProvider client={this.createClient()}>
        <View style={styles.container}>
          <FeedWithData />
        </View>
      </ApolloProvider>
    );
  }
}

const FeedWithData = graphql(gql`
  {
    feed (type: TOP, limit: 5) {
      repository {
        owner { login }
        name
      }
  
      postedBy { login }
    }
  }
`)(Feed);

function Feed({ data }) {
  if (data.loading) {
    return <Text style={styles.loading}>Loading...</Text>;
  }
  
  if (data.error) {
    return <Text>Error! {data.error.message}</Text>;
  }
  
  return (
    <View>
      <Text style={styles.title}>GitHunt</Text>
      { data.feed.map((item) => <FeedItem item={item} />) }
      <Button
        style={styles.learnMore}
        onPress={goToApolloWebsite}
        title="Learn more about Apollo!"/>
    </View>
  );
}

function FeedItem({ item }) {
  return (
    <View style={styles.feedItem}>
      <Text style={styles.entry}>{item.repository.owner.login}/{item.repository.name}</Text>
      <Text>Posted by {item.postedBy.login}</Text>
    </View>
  );
}

function goToApolloWebsite() {
  Linking.openURL('http://dev.apollodata.com').catch((e) => console.log(e));
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
